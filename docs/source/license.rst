=======
License
=======

AltWalker is licensed under the GNU General Public License v3.0.

License Text
------------

You can find the full license text `here <https://gitlab.com/altom/altwalker/altwalker/-/blob/develop/LICENSE>`_.
